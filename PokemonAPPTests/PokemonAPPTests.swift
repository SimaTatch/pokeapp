
import XCTest
import Combine
@testable import PokemonAPP

class PokemonAPPTests: XCTestCase {

    var systemUnderTests: PokeViewModel!
    var pokemonAPIService: MockPokemonAPIService!
    var viewModelOutput: MockPokemonViewModelOutput!
    
    override func setUp() {
        super.setUp()
        pokemonAPIService = MockPokemonAPIService()
        viewModelOutput = MockPokemonViewModelOutput()
        systemUnderTests = PokeViewModel(pokemonAPIService: pokemonAPIService)
        systemUnderTests.output = viewModelOutput
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testUpdatePoke_onViewDidLoadWithSuccessAPI_isCalled() {
//        given
        let pokemons: [Pokemon] = [
            .init(id: 1, name: "bulbasaur", imageUrl: "https://firebasestorage.googleapis.com/v0/b/pokedex-bb36f.appspot.com/o/pokemon_images%2F2CF15848-AAF9-49C0-90E4-28DC78F60A78?alt=media&token=15ecd49b-89ff-46d6-be0f-1812c948e334", description: "Bulbasaur can be seen napping in bright sunlight.\nThere is a seed on its back. By soaking up the sun’s rays,\nthe seed grows progressively larger.", height: 7, weight: 69, attack: 49, defense: 49, type: "poison"),
            .init(id: 5, name: "charmeleon", imageUrl: "https://firebasestorage.googleapis.com/v0/b/pokedex-bb36f.appspot.com/o/pokemon_images%2F80386722-CE53-451C-B974-84964C020298?alt=media&token=c1a8047e-521d-41d2-8a04-d479faecfb07", description: "Charmeleon mercilessly destroys its foes using its sharp claws. If it encounters a strong foe, it turns aggressive. In this excited state, the flame at the tip of its tail flares with a bluish white color.", height: 11, weight: 190, attack: 64, defense: 58, type: "fire")
        ]
        
        pokemonAPIService.fetchPokemonResult = CurrentValueSubject(pokemons).eraseToAnyPublisher()
//        when
        systemUnderTests.onViewDidLoad()
//        then
        XCTAssertEqual(viewModelOutput.updatePokemonArray.count, 2)
        
    }
    
    func testUpdatePoke_onViewDidLoadWithFailedAPI_isNotCalled() {
//        given
        let error = NSError()
        pokemonAPIService.fetchPokemonResult = Fail(error: error).eraseToAnyPublisher()
//        when
        systemUnderTests.onViewDidLoad()
//        then
        XCTAssertEqual(viewModelOutput.updatePokemonArray.count, 1)
    }
}


class MockPokemonAPIService: PokemonAPIService {
    
    var fetchPokemonResult: AnyPublisher<[Pokemon], Error>?
    
    func fetchPokeAPI() -> AnyPublisher<[Pokemon], Error> {
        if let result = fetchPokemonResult {
            return result
        } else {
            fatalError("result must not be nil")
        }
    }
}

class MockPokemonViewModelOutput: PokemonViewModelOutput {
    var updatePokemonArray: [Pokemon] = []
    
    func updatePoke(poke: [Pokemon]) {
        updatePokemonArray.append(contentsOf: poke)
    }
    
    
}
