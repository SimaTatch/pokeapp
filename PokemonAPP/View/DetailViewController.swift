
import Foundation
import UIKit
import Combine
import SDWebImage

class DetailViewController: UIViewController {
    
    //MARK: - Objects & init()
    let pokemon: Pokemon
    let viewModel: PokemonCellViewModel
    var barViewModel: BarChartView
    
    
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
        self.viewModel = PokemonCellViewModel(pokemon: pokemon)
        self.barViewModel = BarChartView(pokemon: pokemon)
        self.pokeName.text = pokemon.name.capitalizingLetter()
        self.pokeDiscription.text = pokemon.description
        self.selectedPokeImage.sd_setImage(with: URL(string: pokemon.imageUrl), completed: nil)
        self.pokeType.text = pokemon.type
        self.pokeType.backgroundColor = viewModel.backgroundColor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Set UIs
    public let pokeName: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .pressStart16
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let selectedPokeImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    public let pokeDiscription: UILabel = {
        let label = UILabel()
        label.textColor = .systemGray5
        label.font = .pressStart14
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let pokeType: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .pressStart10
        label.textAlignment = .center
        label.layer.cornerRadius = 10
        label.adjustsFontSizeToFitWidth = true
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = viewModel.backgroundColor
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [PokemonCellViewModel(pokemon: pokemon), UIColor.white.cgColor]
        view.layer.insertSublayer(gradient, at: 0)
        setupViews()
        setupConstraints()
    }
    
    //MARK: - Funcs
    private func setupViews() {
        view.addSubview(selectedPokeImage)
        view.addSubview(pokeName)
        view.addSubview(pokeType)
        view.addSubview(barViewModel)
        view.addSubview(pokeDiscription)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            selectedPokeImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            selectedPokeImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            selectedPokeImage.heightAnchor.constraint(equalToConstant: 250)
        ])
        
        NSLayoutConstraint.activate([
            pokeName.topAnchor.constraint(equalTo: selectedPokeImage.bottomAnchor, constant: 15),
            pokeName.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pokeName.bottomAnchor.constraint(equalTo: pokeType.topAnchor, constant: -8)
        ])
        
        NSLayoutConstraint.activate([
            pokeType.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pokeType.widthAnchor.constraint(equalToConstant: 100),
            pokeType.heightAnchor.constraint(equalToConstant: 20),
            pokeType.bottomAnchor.constraint(equalTo: pokeDiscription.topAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            pokeDiscription.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5),
            pokeDiscription.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5)
        ])
    }
}
