
import UIKit
import Combine
import SDWebImage

class PokeViewController: UIViewController, PokemonViewModelOutput {
    
    
    //MARK: - Properties
    private let viewModel: PokeViewModel = PokeViewModel(pokemonAPIService: PokemonAPIServiceImp())
    private var dataManager = PokemonAPIServiceImp()
    private let idPokemonCell = "idPokemonCell"
    public var pokemonSubscriber: AnyCancellable?
    public var filteredPokemons = [Pokemon]()
    public var pokemons = [Pokemon]() {
        didSet {
            pokemonCollectionView.reloadData()
        }
    }
    
    //MARK: - Setup SearchBar
    lazy var  pokeSearchController = UISearchController(searchResultsController: nil)
    
    private var searchBarIsEmpty: Bool {
        guard let text = pokeSearchController.searchBar.text else {return false}
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return pokeSearchController.isActive && !searchBarIsEmpty
    }

    //MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        setupDelegates()
        pokemonCollectionView.register(PokemonCell.self, forCellWithReuseIdentifier: idPokemonCell)
        
        viewModel.onViewDidLoad()
        viewModel.output = self
        
        
        self.navigationItem.titleView = pokeSearchController.searchBar
        pokeSearchController.searchBar.placeholder = "Enter pokemon's name here"
        pokeSearchController.searchBar.searchTextField.font = .pressStart10
        pokeSearchController.hidesNavigationBarDuringPresentation = false
        pokeSearchController.obscuresBackgroundDuringPresentation = false
        pokeSearchController.searchResultsUpdater = self
    }
    
    //MARK: - PokemonViewModelOutput
    func updatePoke(poke: [Pokemon]) {
        self.pokemons = poke
    }
    
    //MARK: - setup collectionView()
    private let pokemonCollectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    //MARK: - funcs
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(pokemonCollectionView)
    }
    
    private func setupDelegates() {
        pokemonCollectionView.dataSource = self
        pokemonCollectionView.delegate = self
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            pokemonCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5),
            pokemonCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5),
            pokemonCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10),
            pokemonCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10)
        ])
    }
}


//MARK: - collectionView: -Delegate, -DataSource, -DelegateFlowLayout
extension PokeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard isFiltering else {return self.pokemons.count}
        return filteredPokemons.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idPokemonCell, for: indexPath)  as! PokemonCell
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
//        cell.addShadowToCell()
        
        let pokemon: Pokemon

        if isFiltering {
            pokemon = self.filteredPokemons[indexPath.item]
        } else {
            pokemon = self.pokemons[indexPath.item]
        }
        
        cell.pokeNameLabel.text = pokemon.name.capitalizingLetter()
        cell.pokeDescriptionLabel.text = pokemon.type
        cell.self.backgroundColor = cell.pokeBackgroundColor(forType: pokemon.type) // need to be changed
        cell.pokePhoto = pokemon
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pokemon: Pokemon
        
        if isFiltering {
            pokemon = self.filteredPokemons[indexPath.item]
            let detailVC = DetailViewController(pokemon: pokemon)
            pokeSearchController.present(detailVC, animated: true, completion: nil)
        } else {
            pokemon = self.pokemons[indexPath.item]
            let detailVC = DetailViewController(pokemon: pokemon)
            self.present(detailVC, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: view.frame.width/2 - 20,
               height: 108)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        8
    }
}


extension PokeViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text ?? "")
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        
        filteredPokemons = pokemons.filter({ pokemon in
            guard pokemon.name.lowercased().contains(searchText.lowercased()) else {return false}
            return true
        })
        pokemonCollectionView.reloadData()
    }
}

