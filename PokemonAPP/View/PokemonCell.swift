

import Foundation
import UIKit
import SDWebImage

class PokemonCell: UICollectionViewCell {

    
    // MARK: - Set UIs
     public let pokeNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .pressStart16
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.3
        label.numberOfLines = 1
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public  let pokeDescriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle:.headline)
        label.minimumScaleFactor = 0.3
        label.font = .pressStart10
        label.numberOfLines = 1
        label.textColor = .white
        label.layer.cornerRadius = 10
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public  let pokeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    public let pokemonLabelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.alignment = .top
        stackView.spacing = 0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    //MARK: - sd_setImage
    var pokePhoto: Pokemon! {
        didSet {
            let photoUrl = pokePhoto.imageUrl
            guard let url = URL(string: photoUrl) else {return}
            pokeImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
    //MARK: - Init()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        addSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Funcs
    func pokeBackgroundColor(forType type: String ) -> UIColor {
        switch type {
        case "fire": return .systemRed
        case "poison": return .systemGreen
        case "water": return .systemBlue
        case "electric": return .systemYellow
        case "psychic": return .systemPurple
        case "normal": return .systemOrange
        case "ground": return .systemGray
        case "flying": return .systemTeal
        case "fairy": return .systemPink
        default: return .systemIndigo
        }
    }
    
    private func setupViews(){
        addSubview(pokeNameLabel)
        addSubview(pokeDescriptionLabel)
        addSubview(pokeImageView)
        addSubview(pokemonLabelsStackView)
    }

    private func addSubviews() {
        pokemonLabelsStackView.addArrangedSubview(pokeNameLabel)
        pokemonLabelsStackView.addArrangedSubview(pokeDescriptionLabel)
    }
    
    private func setupConstraints(){
        NSLayoutConstraint.activate([
            pokeDescriptionLabel.widthAnchor.constraint(equalToConstant: self.frame.width),
            pokeDescriptionLabel.heightAnchor.constraint(equalToConstant: self.frame.height/4)
        ])
        
        NSLayoutConstraint.activate([
            pokeNameLabel.widthAnchor.constraint(equalToConstant: self.frame.width),
            pokeNameLabel.heightAnchor.constraint(equalToConstant: self.frame.height - (self.frame.height/4))
        ])
        
        NSLayoutConstraint.activate([
            pokemonLabelsStackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 2),
            pokemonLabelsStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 2),
            pokemonLabelsStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            pokemonLabelsStackView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: CGFloat(0.5))
        ])
        
        NSLayoutConstraint.activate([
            pokeImageView.leadingAnchor.constraint(equalTo: pokemonLabelsStackView.trailingAnchor, constant: 2),
            pokeImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            pokeImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            pokeImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0)
        ])
    }
}

