

import UIKit

class BarView: UIView {
    
    //MARK: - Objects & init()
    var value: Int = Int()
    var title: String = String()
    var color: UIColor = UIColor()
  
    init (value: Int, title: String, color: UIColor) {
        super.init(frame: CGRect(x: 0, y: 0, width: 300, height: 20))
        self.value = value
        self.title = title
        self.color = color

        self.backgroundColor = .clear
        pokeTitle.text = title
        pokeValue.text = String(value)
        pokeProgress.progressTintColor = color
        pokeProgress.setProgress(Float(value)/100, animated: true)
        
        addSubview(pokeTitle)
        addSubview(pokeValue)
        addSubview(pokeProgress)
        pokeStackView.addArrangedSubview(pokeTitle)
        pokeStackView.addArrangedSubview(pokeValue)
        pokeStackView.addArrangedSubview(pokeProgress)
        addSubview(pokeStackView)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Set UIs
    public let pokeTitle: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .left
        label.font = .pressStart12
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let pokeValue: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = .pressStart10
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public let pokeProgress: UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.layer.cornerRadius = 10
        progressView.clipsToBounds = true
        progressView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        progressView.layer.sublayers![1].cornerRadius = 10
        progressView.subviews[1].clipsToBounds = true
        progressView.translatesAutoresizingMaskIntoConstraints = false
        return progressView
    }()
    
    private let pokeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 3
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    //MARK: - Func
    private func setupConstraints() {
        NSLayoutConstraint.activate ([
            pokeTitle.widthAnchor.constraint(equalToConstant: 90),
            pokeTitle.heightAnchor.constraint(equalToConstant: self.frame.height)
        ])
        
        NSLayoutConstraint.activate ([
            pokeValue.widthAnchor.constraint(equalToConstant: 40),
            pokeValue.heightAnchor.constraint(equalToConstant: self.frame.height)
        ])
        
        NSLayoutConstraint.activate ([
            pokeProgress.widthAnchor.constraint(equalToConstant:200),
            pokeProgress.heightAnchor.constraint(equalToConstant: self.frame.height)
        ])
        
        NSLayoutConstraint.activate ([
            pokeStackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            pokeStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
            pokeStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
            pokeStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }
}

