
import Foundation
import UIKit

class BarChartView: UIView {
    
    //MARK: - Objects & init()
    let pokemon: Pokemon
    
    init (pokemon: Pokemon) {
        self.pokemon = pokemon
        super.init(frame: CGRect(x: 40, y: 500, width: 300, height: 100))
        self.backgroundColor = .clear
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        
        pokeStack.addArrangedSubview(BarView(value: pokemon.height, title: "Height", color: .orange))
        pokeStack.addArrangedSubview(BarView(value: pokemon.attack, title: "Attack", color: .red))
        pokeStack.addArrangedSubview(BarView(value: pokemon.defense, title: "Defense", color: .blue))
        pokeStack.addArrangedSubview(BarView(value: pokemon.weight, title: "Weight", color: .purple))
        
        addSubview(pokeStack)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
     //MARK: - Set UI
    private let pokeStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    //MARK: - Func
    private func setupConstraints() {
        NSLayoutConstraint.activate ([
            pokeStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            pokeStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
            pokeStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
            pokeStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }
}
