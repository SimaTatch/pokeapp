
import UIKit
import Combine

protocol PokemonViewModelOutput: AnyObject {
    func updatePoke(poke: [Pokemon])
}

class PokeViewModel {
    
    private let pokemonAPIService: PokemonAPIService
    private var subscribers = Set<AnyCancellable>()
    weak var output: PokemonViewModelOutput?
    
    init(pokemonAPIService: PokemonAPIService){
        self.pokemonAPIService = pokemonAPIService
    }
    
    func onViewDidLoad() {
        displayPokemons()
    }
    
    private func displayPokemons() {
        pokemonAPIService.fetchPokeAPI().sink {[weak self] completion in
            switch completion {
            case .failure:
                let pokemons = [SAMPLE_POKEMON]
                self?.output?.updatePoke(poke: pokemons)
            case .finished: break
            }
        } receiveValue: { [weak self] (pokemons) in
            self?.output?.updatePoke(poke: pokemons)
        }.store(in: &subscribers)
    }
}
