
import Foundation
import Combine
import UIKit

protocol PokemonAPIService {
    func fetchPokeAPI() -> AnyPublisher<[Pokemon],Error>
}

class PokemonAPIServiceImp: PokemonAPIService {
    
    let baseURL = "https://pokedex-bb36f.firebaseio.com/pokemon.json"
    
    func fetchPokeAPI() -> AnyPublisher<[Pokemon],Error> {
        let url = URL(string: baseURL)!
        return URLSession.shared.dataTaskPublisher(for: url)
            .map{$0.data.parseData(removeString: "null,")!}
            .decode(type: [Pokemon].self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}



