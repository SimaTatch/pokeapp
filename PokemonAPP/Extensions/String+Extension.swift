
import Foundation

extension String {
    
    func capitalizingLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
}
