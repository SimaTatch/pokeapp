

import Foundation
import UIKit


extension UIFont {
    
    static func pokeHollow18() -> UIFont? {
        return UIFont.init(name: "Pokemon Hollow", size: 19)
    }
    
    static func pokeSolid18() -> UIFont? {
        return UIFont.init(name: "Pokemon Solid", size: 18)
    }
    
    static let pressStart10 = UIFont(name: "PressStart2P-Regular", size: 10)!
    static let pressStart12 = UIFont(name: "PressStart2P-Regular", size: 12)!
    static let pressStart14 = UIFont(name: "PressStart2P-Regular", size: 14)!
    static let pressStart16 = UIFont(name: "PressStart2P-Regular", size: 16)!
}

